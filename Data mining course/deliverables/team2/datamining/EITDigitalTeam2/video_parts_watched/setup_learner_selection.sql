﻿DROP TABLE IF EXISTS learner_selection;

SELECT
  users.eitdigital_user_id,
  users.reported_or_inferred_gender,
  users.country_cd,
  users.employment_status,
  course_memberships.course_id
INTO learner_selection
FROM users
LEFT JOIN course_memberships ON users.eitdigital_user_id=course_memberships.eitdigital_user_id
WHERE course_membership_role = 'LEARNER';

CREATE INDEX learner_selection_user_id_idx ON learner_selection (eitdigital_user_id);
CREATE INDEX learner_selection_gender_idx ON learner_selection (reported_or_inferred_gender);
CREATE INDEX learner_selection_country_idx ON learner_selection (country_cd);
CREATE INDEX learner_selection_employment_status_idx ON learner_selection (employment_status);
CREATE INDEX learner_selection_course_id ON learner_selection (course_id);

UPDATE learner_selection SET reported_or_inferred_gender = 'unknown' WHERE reported_or_inferred_gender IS NULL OR reported_or_inferred_gender = '';
UPDATE learner_selection SET employment_status = 'UNKNOWN' WHERE employment_status IS NULL OR employment_status = '';
UPDATE learner_selection SET employment_status = 'UNEMPLOYED' WHERE employment_status IN ('UNABLE_TO_WORK','RETIRED');