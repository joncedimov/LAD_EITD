CREATE OR REPLACE FUNCTION video_parts_watched_item(character varying[], character varying[])
RETURNS table(course_item_id character varying, watched_part_mid numeric, num_learners bigint, avg_times_watched numeric) AS $$
SELECT course_item_id, (lower(watched_part) + upper(watched_part)) / 2 AS watched_part_mid, COUNT(hashed_user_id) AS num_learners, AVG(times_watched) AS avg_times_watched
FROM video_parts_watched_learners
WHERE course_item_id LIKE ANY($1) AND hashed_user_id = ANY($2)
GROUP BY course_item_id, watched_part
ORDER BY course_item_id, watched_part
$$ LANGUAGE SQL;
