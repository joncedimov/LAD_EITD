DROP TABLE IF EXISTS clickstream_events_video;

CREATE TABLE clickstream_events_video
(
  hashed_user_id character varying,
  hashed_session_cookie_id character varying,
  server_timestamp timestamp without time zone,
  hashed_ip character varying,
  user_agent character varying,
  url character varying,
  initial_referrer_url character varying,
  browser_language character varying,
  course_id character varying,
  country_cd character varying,
  region_cd character varying,
  timezone character varying,
  os character varying,
  browser character varying,
  key character varying,
  value json
);

\COPY clickstream_events_video FROM 'automata_fullv.csv' CSV NULL '' QUOTE '"' ESCAPE E'\\';
\COPY clickstream_events_video FROM 'iot_fullv.csv' CSV NULL '' QUOTE '"' ESCAPE E'\\';
\COPY clickstream_events_video FROM 'realtime_fullv.csv' CSV NULL '' QUOTE '"' ESCAPE E'\\';

