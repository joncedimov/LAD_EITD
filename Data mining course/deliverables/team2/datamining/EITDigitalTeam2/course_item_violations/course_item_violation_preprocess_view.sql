
CREATE OR REPLACE VIEW course_item_lesson_modules_final AS    
  select 
  course_progress.eitdigital_user_id,
  course_branch_modules.course_branch_id,
        course_branch_modules.course_module_id,
        course_branch_modules.course_branch_module_order, 
        course_branch_lessons.course_lesson_id,
        course_branch_lessons.course_branch_lesson_order,
        course_branch_items.course_item_id, course_branch_items.course_branch_item_order,
max(course_progress.course_progress_ts)
        from course_progress 
INNER JOIN course_branch_items using (course_item_id)
INNER JOIN course_branch_lessons using (course_lesson_id )
INNER JOIN course_branch_modules using (course_module_id) 
where course_progress_state_type_id=2 
GROUP BY 
        course_progress.eitdigital_user_id,
        course_branch_modules.course_branch_id,
        course_branch_modules.course_module_id,
        course_branch_lessons.course_lesson_id,
        course_branch_items.course_item_id, 
        course_branch_modules.course_branch_module_order, 
        course_branch_lessons.course_branch_lesson_order,course_branch_items.course_branch_item_order
ORDER by course_progress.eitdigital_user_id,
        course_branch_modules.course_branch_id,
        course_branch_modules.course_module_id,
        course_branch_lessons.course_lesson_id, max(course_progress.course_progress_ts)
