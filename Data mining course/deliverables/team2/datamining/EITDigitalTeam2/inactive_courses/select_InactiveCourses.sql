-- View: public.course_item_access_data

-- DROP VIEW public.course_item_access_data;

CREATE OR REPLACE VIEW public.course_item_access_data AS 
 SELECT course.course_id,
    module.course_module_id,
    lesson.course_lesson_id,
    item.course_item_id,
    course.course_name,
    module.course_branch_module_name,
    module.course_branch_module_order,
    lesson.course_branch_lesson_name,
    lesson.course_branch_lesson_order,
    item.course_branch_item_name,
    item.course_branch_item_order,
    item_type.course_item_type_desc,
    item_type.course_item_type_category,
    progress.eitdigital_user_id,
    progress.course_progress_ts AS last_accessed_time,
    item_type.course_item_type_id
   FROM courses course
     JOIN course_branches branch ON course.course_id::text = branch.course_id::text
     JOIN course_branch_modules module ON branch.course_branch_id::text = module.course_branch_id::text
     JOIN course_branch_lessons lesson ON module.course_module_id::text = lesson.course_module_id::text
     JOIN course_branch_items item ON item.course_branch_id::text = branch.course_branch_id::text AND item.course_lesson_id::text = lesson.course_lesson_id::text
     JOIN course_item_types item_type ON item.course_item_type_id = item_type.course_item_type_id
     JOIN course_progress progress ON course.course_id::text = progress.course_id::text AND item.course_item_id::text = progress.course_item_id::text AND progress.course_progress_state_type_id = 2
  WHERE course.course_id::text = ANY (ARRAY['Wp6z-dymEeWUwhJ351EtdQ'::character varying::text, 'V4m7Xf5qEeS9ISIACxWDhA'::character varying::text, 'pCa5-xauEeWEjBINzvDOWw'::character varying::text])
  ORDER BY course.course_name, module.course_branch_module_order, lesson.course_branch_lesson_order, item.course_branch_item_order;

ALTER TABLE public.course_item_access_data
  OWNER TO postgres;





-- Function: public.get_inactive_courseitems(integer, timestamp without time zone)

-- DROP FUNCTION public.get_inactive_courseitems(integer, timestamp without time zone);

CREATE OR REPLACE FUNCTION public.get_inactive_courseitems(
    IN v_numweeks integer,
    IN startdate timestamp without time zone)
  RETURNS TABLE(course_name character varying, course_branch_module_name character varying, course_branch_lesson_name character varying, course_branch_item_name character varying, course_item_type_category character varying, weeks double precision) AS
$BODY$
select  
       course_name,
       course_branch_module_name, 
       course_branch_lesson_name, 
       course_branch_item_name,
       coalesce(course_item_type_category, 'Empty') as course_item_type_category ,
       TRUNC(DATE_PART('day',  startdate::timestamp - max(last_accessed_time)::timestamp)/7) as weeks
from course_item_access_data
group by  course_id,
       course_name,
       course_branch_module_name, 
       course_branch_lesson_name, 
       course_item_id, 
       course_branch_item_name,
       course_item_type_desc,
       course_item_type_category
having TRUNC(DATE_PART('day',  startdate::timestamp - max(last_accessed_time)::timestamp)/7)  > v_numWeeks;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.get_inactive_courseitems(integer, timestamp without time zone)
  OWNER TO postgres;



-- Function: public.get_inactive_courseitems_withcourses(integer, timestamp without time zone, character varying[])

-- DROP FUNCTION public.get_inactive_courseitems_withcourses(integer, timestamp without time zone, character varying[]);

CREATE OR REPLACE FUNCTION public.get_inactive_courseitems_withcourses(
    IN v_numweeks integer,
    IN startdate timestamp without time zone,
    IN v_courseids character varying[])
  RETURNS TABLE(course_name character varying, course_branch_module_name character varying, course_branch_lesson_name character varying, course_branch_item_name character varying, course_item_type_category character varying, weeks double precision) AS
$BODY$
select  
       course_name,
       course_branch_module_name, 
       course_branch_lesson_name, 
       course_branch_item_name,
       course_item_type_category,
       TRUNC(DATE_PART('day',  startdate::timestamp - max(last_accessed_time)::timestamp)/7) as weeks
from course_item_access_data
where course_id = ANY(v_courseIDs)
group by  course_id,
       course_name,
       course_branch_module_name, 
       course_branch_lesson_name, 
       course_item_id, 
       course_branch_item_name,
       course_item_type_desc,
       course_item_type_category
having TRUNC(DATE_PART('day',  startdate::timestamp - max(last_accessed_time)::timestamp)/7)  > v_numWeeks;
$BODY$
  LANGUAGE sql VOLATILE
  COST 100
  ROWS 1000;
ALTER FUNCTION public.get_inactive_courseitems_withcourses(integer, timestamp without time zone, character varying[])
  OWNER TO postgres;



