# Repository of Team 1 #

## Installation ##

`
pip install bokeh
pip install psycopg2
`

## Configuration ##
Change the database connection setting  
`
conn = psycopg2.connect("dbname='eit' user='postgres' host='localhost' password='coursera'")
`
## Preapare clickstream data ##

- unzip the clickstream data, it doesnt matter where.

- run the sql script setup_clickstream.sql which creates the tables

- run the load_maker.py script where you have the unzipped csv files

- run the generated load.sql script to load the csv contents into the tables

- run these queries to modify the value column of the tables into json type

ALTER TABLE access_clickstream
ALTER COLUMN value SET DATA TYPE json USING value::json;
ALTER TABLE videos_clickstream
ALTER COLUMN value SET DATA TYPE json USING value::json;


## Run ##

In your Command Prompt:  
`
bokeh serve --show main.py
`
