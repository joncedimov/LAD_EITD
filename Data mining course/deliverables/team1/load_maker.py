import os

#  CREATE THE LOAD
loadfile = open('load.sql','w')
video_table = 'videos_clickstream'
access_table = 'access_clickstream'

for filename in os.listdir('.'):
    if (filename.startswith("video") or filename.startswith("access")) and filename.endswith(".csv"):
        if filename.startswith("video"):
            loadfile.write("\COPY "+ video_table+" FROM '"+filename+ "' CSV NULL '' QUOTE '\"' ESCAPE '\\';" +'\n')
        else:
            loadfile.write("\COPY " + access_table + " FROM '" + filename + "' CSV NULL '' QUOTE '\"' ESCAPE '\\';" + '\n')
loadfile.close()